globals [
  wordlist
 engwordlist
 danwordlist
 chosenengwordlist
 chosendanwordlist
 eventword
  change
  np
  nm
  hugeweightlist
  hugedanweightlist
  hugeengweightlist
  hugelanguagenumberlist

  spokendannumber
  spokenengnumber
  percentdan
  percenteng
    winspokendannumber
    winpercentdan
    winspokenengnumber
    winpercenteng

  totalevents
  eventwordslist

  nrdanturtles
  nrengturtles
  percentdanturtles
  percentengturtles
    winnrdanturtles
    winnrengturtles
    winpercentdanturtles
    winpercentengturtles

  spokenwordslist
   nicecount
   coolcount
   funkycount
   awesomecount
   sweetcount
    fedtcount
    lækkertcount
    dejligtcount
    sejtcount
    skøntcount
]

turtles-own [
 partner
 turtlelist
 turtlewordlist
 turtletable
 threshold
 oldweight
 newweight
  shortturtlewordlist
  shortlist
 newweights
 turtlemeanweight
 these-weights
 these-weights-dan
 these-weights-eng
 turtledanmeanweight
 turtleengmeanweight

 mymeandanweight
 mymeanengweight
 mylanguage
]

to setup
  clear-all
  set hugeweightlist []
  set hugedanweightlist []
  set hugeengweightlist []
  set hugelanguagenumberlist []
  set eventwordslist []
  set spokenwordslist []
  set np pos-step
  set nm neg-step
  ;;set eventnumber (N / 2)
  ask patches [ set pcolor white ]

 ;;  set engwordlist of possible English words to be chosen (arbitrary, but fun):
  set engwordlist [ ["nice" "eng"] ["cool" "eng"] ["funky" "eng"] ["awesome" "eng"] ["sweet" "eng"] ]
    ;;["brilliant" "eng"] ["dope" "eng"]  ["win" "eng"]  ["sweet" "eng"] ["supercalifragilisticexpialidocious" "eng"] ["delightful" "eng"]

   ;;# set danwordlist of possible Danish words to be chosen
  set danwordlist [ ["fedt" "dan"] ["lækkert" "dan"] ["dejligt" "dan"]  ["sejt" "dan"]  ["skønt" "dan"] ]
    ;;["smart" "dan"] ["fantastisk" "dan"] ["flot" "dan"] ["svedigt" "dan"] ["knæhøj karse" "dan"] ["kodylt" "dan"] ["oppern" "dan"] ["optur" "dan"] ["super" "dan"]
    ;; ["fantalistisk" "dan"] ["vildt" "dan"] ["kanon" "dan"] ["bred ymer" "dan"] ["fedest" "dan"] ["overdrevet" "dan"]
    ;;["spa" "dan"] ["luksus" "dan"] ["superkolofantalistieksplialisofisk" "dan"]

;;#choose a nr of words from each language at random:
  set chosenengwordlist n-of ewN engwordlist ;;eWn and dWn are numbers specified in the interface - the wanted number of English/Danish words
  set chosendanwordlist n-of dwN danwordlist
  set wordlist sentence chosendanwordlist chosenengwordlist ;;# create the full two-element wordlist with first all Danish, then all English words

construct-agents
reset-ticks
end

to construct-agents
  create-turtles N [setxy random-xcor random-ycor] ;;must be even nr of turtles
  ask turtles [set shape "person" set color black

;;CONSTRUCT INVENTORY:
;;create the weightlist for the Danish words:
    let danweightlist range length chosendanwordlist ;;same length as nr of chosen Danish words (one weight for each word)
  let danweightresult []

foreach danweightlist [
   set danweightresult lput (random-normal-in-bounds dM dSD 0.0000000000000001 0.9999999999999999) danweightresult ;;this function makes sure it's between 0 and 1
    ]                                               ;;^^OBS: change this distribution to see what happens

 ;;and for the English words:
 let engweightlist range length chosenengwordlist ;;same length as nr of chosen English words (one weight for each word)
  let engweightresult []
 foreach engweightlist [
      set engweightresult lput (random-normal-in-bounds eM eSD 0.0000000000000001 0.9999999999999999) engweightresult
    ]                                                  ;;^^OBS: change this distribution to see what happens

   let weightresult sentence danweightresult engweightresult ;;combine the two weight lists (Danish weights first, then English weights)

;;make a complete turtlewordlist with word, language, weight for each turtle:
  set turtlewordlist (
    map lput weightresult wordlist) ;;these magic lines puts a weight as the third element in each word list (Danish weights for Danish words, English for English)
  ] ;;CONSTRUCT-AGENTS ENDS HERE
end

to talk
;;CHOOSE A PARTNER:
 ask one-of turtles [ if partner = 0 [ ;; partner = 0 at first
      set partner one-of other turtles with [ partner = 0 ]
      ask partner [ set partner myself ] ;;both store the other turtle in partner
    create-link-with partner ;;just to visualize it
    let currentlink one-of links ;;just to visualize - since there is only one link at a time
    ask currentlink [set color red]

;;SET SPEAKER AND HEARER:
  let pair [who] of [both-ends] of currentlink
  let switch random 2
  let speaker item switch pair
  let hearer item (1 - switch) pair
  set speaker turtle speaker
  set hearer turtle hearer ;;one speaker, one hearer
;;  ask speaker [set color yellow] ask hearer [set color pink] ;;JUST VISUALIZING

;;TALK:
 let chosenword []
    let chosenwordlang []
    let chosenwordlangnumberform []

ask speaker [
  ;;SPEAKER CHOOSES A WORD ACCORDING TO A THRESHOLD AND RANDOMNESS:
  set chosenword word-from-threshold ;;function, chooses a random word above a randomly created threshold

  ;;for keeping track of spoken words
  let chosenwordword item 0 chosenword
      set spokenwordslist lput chosenwordword spokenwordslist



   ;;function outputs: [item 0 item 1]
      set chosenwordlang item 1 chosenword

      ifelse chosenwordlang = "dan" [set chosenwordlangnumberform 0] [set chosenwordlangnumberform 1]
      set hugelanguagenumberlist lput chosenwordlangnumberform hugelanguagenumberlist

 ;;keeping count of how many English and Danish words have been spoken in the global variables for plotting:
 ifelse chosenwordlang = "dan"
      [set spokendannumber (spokendannumber + 1) set winspokendannumber (winspokendannumber + 1)]
      [set spokenengnumber (spokenengnumber + 1) set winspokenengnumber (winspokenengnumber + 1)]

  if ticks > 0 [if ticks mod winnumber = 0 [set winspokendannumber 1 set winspokenengnumber 1]] ;;if winnumber nr of conversations have been held, reset (just for plotting)

 set percenteng ((spokenengnumber / (spokendannumber + spokenengnumber)) * 100)  ;;calculate percent spoken English words
 set percentdan ((spokendannumber / (spokendannumber + spokenengnumber)) * 100)       ;;and Danish
set winpercenteng ((winspokenengnumber / (winspokendannumber + winspokenengnumber)) * 100) ;;English rolling window percentage
set winpercentdan ((winspokendannumber / (winspokendannumber + winspokenengnumber)) * 100) ;;Danish

;; if ticks > 0 [if ticks mod eventnumber = 0 [event]] ;;if it's not the very first round (ticks > 0) and there have been as many pairs as half the turles, EVENT TIME!


 set chosenword item 0 chosenword ;;let the chosen word be just the word

  ;;increase weight of chosenword:
 set newweights []

 foreach turtlewordlist [i ->
   let current-word2 item 0 i
      ifelse current-word2 = chosenword

       ;;if it matches chosenword:
         [set oldweight item 2 i
           logit ;;puts oldweight through logit, giving change a new value
           set change change + np ;;increase change by positive step size np
           sigm ;;uses the value of change to set a new value for newweight
           set newweights lput newweight newweights] ;;add weight to newweights list

       ;;if it does not match, i.e. must decrease in weight:
        [set oldweight item 2 i
           logit
           set change change - nm
           sigm ;;sets new value for newweight
           set newweights lput newweight newweights ;;add weight to newweights list
        ]
      ]  ;;LOOP ENDS HERE

  ;;delete old weights:
  set shortturtlewordlist []
    foreach turtlewordlist [i ->
       set shortlist []

        set shortlist remove-item 2 i
        set shortturtlewordlist lput shortlist shortturtlewordlist
      ]

  ;;combine the newweights list with the turtlewordlist:
   set turtlewordlist (
     map lput newweights shortturtlewordlist)

   ;;REMOVE PARTNER!!! OBS


      ] ;;ASK SPEAKER ENDS HERE


 ask hearer[

 ;;increase weight of chosenword
 set newweights []

 foreach turtlewordlist [i ->
   let current-word2 item 0 i
      ifelse current-word2 = chosenword

       ;;if it matches chosenword:
         [set oldweight item 2 i
           logit ;;puts oldweight through logit, giving change a new value
           set change change + np ;;increase change by positive step size np
           sigm ;;uses the value of change to set a new value for newweight
           set newweights lput newweight newweights] ;;add weight to newweights list

       ;;if it does not match, i.e. must decrease in weight:
        [set oldweight item 2 i
           logit
           set change change - nm
           sigm ;;sets new value for newweight
           set newweights lput newweight newweights ;;add weight to newweights list
        ]
      ]  ;;LOOP ENDS HERE

  ;;delete old weights:
 set shortturtlewordlist []
    foreach turtlewordlist [i ->
     set shortlist []

        set shortlist remove-item 2 i
        set shortturtlewordlist lput shortlist shortturtlewordlist
      ]

  ;;combine the newweights list with the turtlewordlist:
   set turtlewordlist (
     map lput newweights shortturtlewordlist)

    ]

;;REMOVE LINK, REMOVE PARTNER, PARTNER = 0, REMOVE COLORS
set speaker []
  set hearer []

    ] ;;ASK-ONE-OF-TURTLES ENDS HERE
] ;;IF PARTNER = 0 ENDS HERE

  ask turtles [set partner 0]
end

to logit
  set change ln (oldweight / (1 - oldweight))
end

to sigm
  set newweight (1 / (1 + exp (- change)))
end


to event
  let pword random 100
   ;;(ALWAYS THINK ONE LESS PERCENT, SINCE 0 IS INCLUDED)
  if pword <= eventlang [ set eventword one-of chosenengwordlist set eventword item 0 eventword] ;;eventlang is a number (if 49 = 50%!)
  if pword > eventlang [ set eventword one-of chosendanwordlist set eventword item 0 eventword]
  ;;(LOOK AT STATS ABOUT CINEMA, DANISH/ENGLISH, ETC.)

  ask (n-of (turtlepercent * count turtles) turtles) [ ;;CAN CHANGE THIS PERCENTAGE IN THE INTERFACE
    set color green ;;to visualize

 ;;increase weight of eventword
 set newweights []

 foreach turtlewordlist [i ->
   let current-word2 item 0 i
      ifelse current-word2 = eventword

       ;;if it matches eventword:
         [set oldweight item 2 i
           logit ;;puts oldweight through logit, giving change a new value
           set change change + np ;;increase change by positive step size np
           sigm ;;uses the value of change to set a new value for newweight
           set newweights lput newweight newweights] ;;add weight to newweights list

       ;;if it does not match, i.e. must decrease in weight:
        [set oldweight item 2 i
           logit
           set change change - nm
           sigm ;;sets new value for newweight
           set newweights lput newweight newweights ;;add weight to newweights list
        ]
      ]  ;;LOOP ENDS HERE

  ;;delete old weights:
  set shortturtlewordlist []
    foreach turtlewordlist [i ->
       set shortlist []
        set shortlist remove-item 2 i
        set shortturtlewordlist lput shortlist shortturtlewordlist
      ]

  ;;combine the newweights list with the turtlewordlist:
   set turtlewordlist (
     map lput newweights shortturtlewordlist)
  ]

set totalevents totalevents + 1
set eventwordslist lput eventword eventwordslist

end

to go
  clear-links
  set eventword []
  clear-counts ;;set all word counts (in count_words) to be empty again (just to make plot making and data processing easier)
  ask turtles [set color black]
  talk ;;right now they also talk in the rounds where there is an event- but we could change that
  if ticks > 0 [if ticks mod eventnumber = 0 [event]] ;;if it's not the very first round (ticks > 0) and there have been as many pairs as half the turles, EVENT TIME!
  ;;important with order of talk and event?
  ask turtles [calculate-means]
  calculate-stuff ;;don't change the order of these two calculations! (otherwise these-weights-dan fucks up)
  set-turtle-color
  calculate-languages
  if length spokenwordslist = 250 [count_words] ;;counts each word's frequency each 250th conversation, resets each time
  tick
 if totalevents = eventlimit [stop]
end


to-report frequency [aword alist] ;;function for calculating frequency of each word in count_words below
    report length filter [i -> i = aword] alist
end

to count_words ;;ugly way to do it - but it works. makes a variable with the nr of times each word has been spoken in the world over the past 125 conversations
   set nicecount frequency "nice" spokenwordslist
   set coolcount frequency "cool" spokenwordslist
   set funkycount frequency "funky" spokenwordslist
   set awesomecount frequency "awesome" spokenwordslist
   set sweetcount frequency "sweet" spokenwordslist
   set fedtcount frequency "fedt" spokenwordslist
   set lækkertcount frequency "lækkert" spokenwordslist
   set dejligtcount frequency "dejligt" spokenwordslist
   set sejtcount frequency "sejt" spokenwordslist
   set skøntcount frequency "skønt" spokenwordslist

 set spokenwordslist []  ;;resets each 250th conversation
end

to clear-counts
  set nicecount []
   set coolcount []
   set funkycount []
   set awesomecount []
   set sweetcount []
   set fedtcount []
   set lækkertcount []
   set dejligtcount []
   set sejtcount []
   set skøntcount []
end



to set-turtle-color
ask turtles [
    let mydanweights []
    let myengweights []

    foreach turtlewordlist [i -> ;;loop through all the nested lists (representing words)
     let current-lang item 1 i ;;the language
     let current-weight item 2 i ;;the weight
      ifelse current-lang = "dan"
        [set mydanweights lput current-weight mydanweights]
        [set myengweights lput current-weight myengweights]
    ] ;;LOOP ENDS

;;look at mean weight for English and Danish words
 set mymeandanweight mean mydanweights
 set mymeanengweight mean myengweights

ifelse mymeandanweight > mymeanengweight
    [set mylanguage 0 set color red]
    [set mylanguage 1 set color blue]

if mymeandanweight = mymeanengweight
    [set mylanguage random 2] ;;choose random language in the unlikely event that the weights have exactly the same mean

  ]
end

to calculate-languages
  set nrdanturtles (count turtles with [mylanguage = 0])
  set nrengturtles (count turtles with [mylanguage = 1])
  set winnrdanturtles (count turtles with [mylanguage = 0])
  set winnrengturtles (count turtles with [mylanguage = 1])

if ticks > 0 [if ticks mod winnumber = 0 [set winnrdanturtles 1 set winnrengturtles 1]] ;;if balbmalnlqanbaw, reset (just for plotting)

set percentengturtles ((nrengturtles / (nrdanturtles + nrengturtles)) * 100)  ;;calculate percent spoken English words
set percentdanturtles ((nrdanturtles / (nrdanturtles + nrengturtles)) * 100)       ;;and Danish

set winpercentengturtles ((winnrengturtles / (winnrdanturtles + winnrengturtles)) * 100) ;;English rolling window percentage
set winpercentdanturtles ((winnrdanturtles / (winnrdanturtles + winnrengturtles)) * 100) ;;Danish


end



to-report random-normal-in-bounds [mid dev mmin mmax]
  let result random-normal mid dev
  if result < mmin or result > mmax
    [report random-normal-in-bounds mid dev mmin mmax]
  report result
end


to-report word-from-threshold
 set threshold random 10000 / 10000 ;;sets random decimal nr between 0 and 1 with 4 decimals
   let possiblewords []
   let result []
  let testtest []

 foreach turtlewordlist [i -> ;;loop through all the nested lists (representing words)
     let current-weight item 2 i ;;the weight of the word
     let current-word item 0 i ;;the word itself
     set testtest remove-item 2 i  ;;list with only word and language

        if current-weight > threshold
        [set possiblewords lput testtest possiblewords] ;;if weight above threshold, select the word for the list
        ] ;;LOOP ENDS HERE
 ifelse length possiblewords = 0
    [report word-from-threshold] ;;if no words above threshold, run the function again, new threshold
    [set result one-of possiblewords] ;;if words above threshold, choose one of them randomly
    report result
end

;;-----------------PLOT STUFF---------------------
to calculate-means
  set these-weights []
  set these-weights-dan []
  set these-weights-eng []

  foreach turtlewordlist [i ->
   let this-weight item 2 i
   set these-weights lput this-weight these-weights] ;;list of weights

  set turtlemeanweight mean these-weights

 ;;mean dan and eng weights:
foreach turtlewordlist [i ->
   let current-language item 1 i

  ifelse current-language = "dan"
    ;;if it is Danish
      [let this-weight-dan item 2 i
      set these-weights-dan lput this-weight-dan these-weights-dan] ;;list of weights

    ;;if it is English:
   [let this-weight-eng item 2 i
      set these-weights-eng lput this-weight-eng these-weights-eng]

 ] ;;LOOP ENDS HERE
  set turtledanmeanweight mean these-weights-dan
  set turtleengmeanweight mean these-weights-eng
end

to calculate-stuff
  set hugeweightlist []
  set hugedanweightlist []
  set hugeengweightlist []
  ask turtles [

  foreach these-weights [i ->
    set hugeweightlist lput i hugeweightlist
  ]

  foreach these-weights-dan [i ->
    set hugedanweightlist lput i hugedanweightlist
  ]

  foreach these-weights-eng [i ->
    set hugeengweightlist lput i hugeengweightlist
  ]
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
260
10
589
340
-1
-1
9.73
1
10
1
1
1
0
0
0
1
-16
16
-16
16
0
0
1
ticks
30.0

BUTTON
22
21
85
54
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
94
21
169
54
go once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
20
70
70
130
N
100.0
1
0
Number

BUTTON
182
19
245
52
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
825
10
991
130
Mean weight value
Ticks
Mean weight
0.0
10.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot mean [turtlemeanweight] of turtles"
"pen-1" 1.0 0 -7500403 true "" "plot median [turtlemeanweight] of turtles"

PLOT
439
345
599
465
Mean weights per language
Ticks
Mean weight
0.0
10.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -2674135 true "" "plot mean [turtledanmeanweight] of turtles"
"pen-1" 1.0 0 -13082198 true "" "plot mean [turtleengmeanweight] of turtles"
"pen-2" 1.0 0 -10873583 true "" "plot median [turtledanmeanweight] of turtles"
"pen-3" 1.0 0 -15390905 true "" "plot median [turtleengmeanweight] of turtles"

SLIDER
11
186
233
219
pos-step
pos-step
0
0.5
0.089
.001
1
NIL
HORIZONTAL

SLIDER
11
226
233
259
neg-step
neg-step
0
0.5
0.01
.001
1
NIL
HORIZONTAL

PLOT
995
10
1255
150
Distribution of weight values
Weight values
Number
0.0
1.0
0.0
10.0
true
false
"" ""
PENS
"default" 0.001 1 -16777216 true "" "histogram hugeweightlist"

PLOT
1071
337
1249
469
Danish weight values
Weight values
Number
0.0
1.0
0.0
10.0
true
false
"" ""
PENS
"default" 0.001 1 -2674135 true "" "histogram hugedanweightlist"

PLOT
899
337
1073
469
English weight values
Weight values
Number
0.0
1.0
0.0
10.0
true
false
"" ""
PENS
"default" 0.001 1 -13345367 true "" "histogram hugeengweightlist"

INPUTBOX
187
266
237
326
ewN
5.0
1
0
Number

INPUTBOX
133
266
183
326
dwN
5.0
1
0
Number

SLIDER
19
139
241
172
turtlepercent
turtlepercent
0
1
0.5
.01
1
NIL
HORIZONTAL

INPUTBOX
78
71
161
131
eventnumber
25.0
1
0
Number

INPUTBOX
8
339
58
399
dM
0.6
1
0
Number

INPUTBOX
65
339
116
399
dSD
0.2
1
0
Number

INPUTBOX
133
341
183
401
eM
0.4
1
0
Number

INPUTBOX
189
341
239
401
eSD
0.2
1
0
Number

INPUTBOX
173
73
240
133
eventlang
69.0
1
0
Number

PLOT
614
157
814
307
Number of spoken words
left = dan, right = eng
Count
0.0
2.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "histogram hugelanguagenumberlist"

PLOT
617
10
807
158
Spoken words per language
Ticks
Count
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -2674135 true "" "plot spokendannumber"
"pen-1" 1.0 0 -13345367 true "" "plot spokenengnumber"

MONITOR
823
153
891
198
Pct eng words
(spokenengnumber / (spokendannumber + spokenengnumber)) * 100
5
1
11

PLOT
816
196
1016
346
Pct spoken words per lang
NIL
NIL
0.0
10.0
0.0
100.0
true
false
"" ""
PENS
"default" 1.0 0 -2674135 true "" "plot percentdan"
"pen-1" 1.0 0 -13345367 true "" "plot percenteng"

INPUTBOX
959
138
1032
198
winnumber
1000.0
1
0
Number

PLOT
1033
162
1233
312
Rolling window pct
Ticks
Pct
0.0
10.0
0.0
100.0
true
false
"" " if ticks > 0 [if ticks mod winnumber = 0 [clear-plot]]"
PENS
"default" 1.0 0 -2674135 true "" "plot winpercentdan"
"pen-1" 1.0 0 -13345367 true "" "plot winpercenteng"

MONITOR
890
152
959
197
Win pct eng
winpercenteng
5
1
11

INPUTBOX
15
270
94
330
eventlimit
500.0
1
0
Number

PLOT
613
309
813
459
Percent turtles speaking
Ticks
Pct turtles speaking langs
0.0
10.0
0.0
100.0
true
false
"" ""
PENS
"default" 1.0 0 -2674135 true "" "plot percentdanturtles"
"pen-1" 1.0 0 -13345367 true "" "plot percentengturtles"

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT
(how to use the model, including a description of each of the items in the Interface tab)


turtlepercent = percent of turtles affected by each event.

eventnumber = if eventnumber mod ticks = 0, an event happens. So if it's 5, an event occurs every 5th tick (starting at tick number 6).
If eventnumber = N/2 (half the number of turtles), it reflects the assumption that an event happens once there has been the same number of conversations as there are turtles.

eventlang = chance of English eventword. If 49 = 50% for English. If 59 = 60% English etc...


## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.4
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="vary_eventlang" repetitions="5000" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>mean [turtlemeanweight] of turtles</metric>
    <metric>median [turtlemeanweight] of turtles</metric>
    <metric>hugeweightlist</metric>
    <metric>hugedanweightlist</metric>
    <metric>hugeengweightlist</metric>
    <metric>spokendannumber</metric>
    <metric>spokenengnumber</metric>
    <metric>percentdan</metric>
    <metric>percenteng</metric>
    <metric>percentdanturtles</metric>
    <metric>percentengturtles</metric>
    <enumeratedValueSet variable="dSD">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dwN">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="winnumber">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="eSD">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="eventlimit">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="neg-step">
      <value value="0.01"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ewN">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="turtlepercent">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="N">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dM">
      <value value="0.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pos-step">
      <value value="0.089"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="eM">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="eventlang">
      <value value="49"/>
      <value value="59"/>
      <value value="74"/>
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="eventnumber">
      <value value="50"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
